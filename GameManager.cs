﻿using Assets.Scripts;
using Assets.Scripts.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public bool isLoadFromGameScene = false;

    private static GameManager instance;
    public int cmi = -1; // currentMissionIndex;
    public static Action<bool> QuestResultAction;
    public class LevelsGlobal
    {
        public bool isMissionComplete = false;
        public ILevel level;
        public LevelGlobalMethods methods;
    }

    public ILevel currentLevel
    {
        get
        {
            if (isLoadFromGameScene)
            {
                return levelsGlobalClass[0].level;
            }
            else
            {
                return levelsGlobalClass[cmi].level;
            }
        }

        set
        {
            if (isLoadFromGameScene)
            {
                levelsGlobalClass[0].level = value;
            }
            else
            {
                levelsGlobalClass[cmi].level = value;
            }
        }
    }

    public LevelsGlobal[] levelsGlobalClass;

    public SoundManager soundManager;
    public SpritesContainer spritesContainer;
    //TODO: LOAD GAME SCENE(take your mission)
    private Missions _missionCache = Missions.Mission7;
    public Missions missionCache
    {
        get { return _missionCache; } 
        set { _missionCache = value; }
    }

    private Characters _character = Characters.BigNose;
    public Characters Character
    {
        get { return _character; }
        set { _character = value; }
    }

    private int _missionState;

    public QuestionPanel questionPanel;
    public ResultPanel resultPanel;
    public QuestPanel questPanel;
    public CompletePanel completePanel;
    public Panel_CameraPos cameraPosPanel;

    Image _imageFade;

    public bool[] missionComplite;

    public static GameManager Instance
    {
        get
        {
            //if (instance == null)
            //{
            //    instance = GameObject.Find("Managers").GetComponent<GameManager>();
            //}
            return instance;
        } 
    }
    private void Awake()
    {
        if (Instance == null)
        {
            instance = this;
            spritesContainer = instance.gameObject.GetComponent<SpritesContainer>();
            levelsGlobalClass = new LevelsGlobal[8];
            for (int i = 0; i < levelsGlobalClass.Length; i++)
            {
                levelsGlobalClass[i] = new LevelsGlobal();

            }
            levelsGlobalClass[0].isMissionComplete = true;
            DontDestroyOnLoad(this.gameObject);

            if (isLoadFromGameScene)
            {

                LinkOfficeObjects(0);
                LinkUI();
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private int lvlIndex;
    private AsyncOperation scene;
    private Loading loading;

    public void LoadScene(int index)
    {
        if (index == 0)
        {
            missionCache = 0;
        }
        lvlIndex = index;
        SceneManager.LoadScene(1);

    }

    private void OnLevelWasLoaded(int level)
    {
        if (Instance != this)
        {
            return;
        }
        if (level == 1)
        {
            StartCoroutine("Load");
            return;
        }
        else if(level == 0)
        {
            loading = null;
            return;
        }

        level -= 2;
        cmi = level;
        switch (level)
        {
            case 0:
                loading = null;
                LinkOfficeObjects(level);
                break;
            default:
                break;
        }

        LinkUI();
    }

    private IEnumerator Load()
    {
        loading = GameObject.Find("Canvas").GetComponent<Loading>();

        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(lvlIndex);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            loading.ShowProcent(asyncOperation.progress);
            if (asyncOperation.progress >= 0.9f)
            {
                loading.ShowProcent("100%");
                    asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    private void LinkUI()
    {
        questionPanel = new QuestionPanel();
        questionPanel.ShowHide(false);
        ResetSprites();
        resultPanel = new ResultPanel();
        resultPanel.ShowHide(false);
        questPanel = new QuestPanel();
        completePanel = new CompletePanel();
        completePanel.ShowHide(false);
        cameraPosPanel = new Panel_CameraPos();

        _imageFade = GameObject.Find("Canvas").transform.Find("BlockPanel").GetComponent<Image>();
        _imageFade.gameObject.SetActive(false);
        var trigger = _imageFade.gameObject.AddComponent<EventTrigger>();
        var onDown = new EventTrigger.Entry { eventID = EventTriggerType.PointerClick };
        onDown.callback.AddListener((eventData) => ClosePanels());
        trigger.triggers.Add(onDown);

        questPanel.AddNewQuest(0);
    }

    private void LinkOfficeObjects(int index)
    {
        var playerMover = GameObject.Find("PlayerMover").gameObject;
        for (int i = 0; i < playerMover.transform.childCount; i++)
        {
            playerMover.transform.GetChild(i).gameObject.SetActive(false);
        }
        playerMover.transform.GetChild((int)Character).gameObject.SetActive(true);

        GameObject environment = GameObject.Find("Environment");
        levelsGlobalClass[index].level = new Level(environment);
        levelsGlobalClass[index].level.Start();
        levelsGlobalClass[index].methods = new LevelMethods();

        missionComplite = new bool[12] { false, false, false, false, false, false, false, false, false, false, false, false };

        soundManager.PlaySound(AudioEnums.Background, true);
        soundManager.PlaySound(AudioEnums.Keyboard, true);
    }

    void Update()
    {
        if (cmi >= 0)
        {
            levelsGlobalClass[cmi].level.Update();
        }

        
    }

    public void ActivateMissionCompletePanel()
    {
        completePanel.ShowHide(true);
    }

    public void StartMission(Missions mission)
    {
        currentLevel.CharacterAnimatorSetValue(AnimatorAction.SetInteger, new object[] { "Mission", (int)mission });
        currentLevel.SetActiveGameObjectMass(GameObjectType.HintParticles, false);
    }

    public void ShowMissionDescription(int i)
    {
        Missions mission = (Missions)i;
        if (!_imageFade.gameObject.activeSelf)
        {
            _imageFade.gameObject.SetActive(true);
            _missionCache = mission;
            ShowMissionMenu(i);
        }
    }

    public void ChangeHintParticle(int i)
    {
        currentLevel.SetActiveGameObjectMass(GameObjectType.HintParticles, false);
        currentLevel.SetActiveGameObject(GameObjectType.HintParticles, i, true);
    }

    public void ShowMissionMenu(int index)
    {
        questionPanel.SetData(index);
        questionPanel.ShowHide(true);
    }

    public void ResetSprites()
    {
        questionPanel.ResetSprites();
    }

    public void ShowMissionResult(int index)
    {
        //completePanel.GoToMenu();

        _missionState = index;
        currentLevel.CharacterAnimatorSetValue(AnimatorAction.SetInteger, new object[] { "MissionState", index });
        _imageFade.gameObject.SetActive(false);
    }

   
    public void ShowHint()
    {
        if (missionComplite != null && !missionComplite[(int)_missionCache])
        {
            currentLevel.SetActiveGameObject(GameObjectType.Missions, (int)_missionCache, true);
        }
    }

    public void ClosePanels()
    {
        if (!questionPanel.Enable())
        {
            _imageFade.gameObject.SetActive(false);
            resultPanel.ShowHide(false);
        }
    }
}
